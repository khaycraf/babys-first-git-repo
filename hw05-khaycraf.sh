#!/usr/bin/bash
#Get a temp read -p "enter temp" temp
#use the formula to change it F - C ($temp - 32) * 1.8  C- F ($temp * .56) + 32, Kelvin is temp + 273.15
#echo result
#decided to get all conversions of a specific place in the world with curl. Used read to add the city to the end of wttr.in to get an entire forecast. Then used grep to only output what I want. Used head to just get the current temp, and finally used tr to make the output just a number.
#Math here was annoying because of decimals and bash. Ended up pipelining into the bc command to count the decimals
read -p "Enter a city you want to get the temp of" city
f="$(curl -s "wttr.in/"$city | grep -Po "(m[0-9][0-9])" | head -n 1 | tr -d "m")"
c="$(echo "($f - 32) / 1.8" | bc)"
k="$(echo "$c + 273.15" | bc)"
echo $f" Farenheit"
echo $c" Celcius"
echo $k" Kelvin"
